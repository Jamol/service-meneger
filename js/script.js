jQuery(document).ready(function($){
	$(".services-toggle").click(function(event){
		if($(this).next().css("display") == "none") {

				$(".services-body:visible")
				.slideUp(500, 'linear', function(){});
		
				$(this).next()
				.slideDown(500, 'linear', function(){});
		}
	});

	$(".mobile__menu-button").click(function(){
		var v = $(".block-menu");
		if(v.css("display") != "none"){
			v.fadeOut(100, 'linear', function(){
			});
		}
		else {
			v.fadeIn(100, 'linear', function(){
			});
		}
	});
})